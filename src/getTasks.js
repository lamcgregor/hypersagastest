function fakeApiCall() {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve("totallyuniqueuserid");
    }, 3000);
  });
}

function secondFakeApiCall() {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve([
        "External one",
        "External two",
        "External three",
        "External four"
      ]);
    }, 6000);
  });
}

export default async function getTasks() {
  const id = await fakeApiCall();
  const items = await secondFakeApiCall(id);
  return items;
}
