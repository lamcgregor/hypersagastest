import React, { Component, Fragment } from "react";

class App extends Component {
  render() {
    const { tasks, actions, fetching } = this.props;
    if (fetching) return <div>Loading...</div>;
    return (
      <Fragment>
        <ul>
          {tasks.map((item, i) => (
            <li key={i}>
              {item}
              <button onClick={() => actions.delete(i)}>-</button>
            </li>
          ))}
        </ul>
        <button onClick={() => actions.fetchTasks()}>Refresh</button>
      </Fragment>
    );
  }
}

export default App;
