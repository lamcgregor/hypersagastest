import getTasks from "./getTasks";

export const initalState = {
  items: 0,
  tasks: [],
  fetching: false
};

const actionsMap = {
  initialise: () => (_, { fetchTasks }) => {
    // fetchTasks();
    return initalState;
  },
  add: task => ({ items, tasks, fetching }) => ({
    items: items + 1,
    tasks: tasks.concat(task),
    fetching
  }),
  delete: index => ({ items, tasks, fetching }) => {
    tasks.splice(index, 1);
    return {
      items: items - 1,
      tasks,
      fetching
    };
  },
  fetchDone: () => state => ({
    ...state,
    fetching: false
  }),
  fetchTasks: () => (state, { add, fetchDone }) => {
    getTasks()
      .then(items => {
        fetchDone(false);
        return items;
      })
      .then(items => {
        items.map(item => add(item));
      });
    return {
      ...state,
      fetching: true
    };
  }
};

export default actionsMap;
