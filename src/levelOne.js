import React from "react";
import App from "./App";
import { makeContextConsumer } from "hyper-redux";

const AppContext = makeContextConsumer();

const levelOne = () => (
  <AppContext>
    {({ actions, state: { tasks, items, fetching } }) => (
      <App actions={actions} items={items} tasks={tasks} fetching={fetching} />
    )}
  </AppContext>
);

export default levelOne;
