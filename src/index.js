import React from "react";
import "./index.css";
import LevelOne from "./levelOne";
import * as serviceWorker from "./serviceWorker";
import { app } from "hyper-redux";

import actions, { initalState } from "./actionReducers";

function view({ items, tasks, fetching }, actions) {
  return (
    <div>
      <p> List has {items} items</p>
      <button onClick={() => actions.add(`Item ${items + 1}`)}>+</button>
      <LevelOne />
    </div>
  );
}

app(initalState, actions, view, document.getElementById("root"));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
